package com.sap.sapbe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SapBeApplication {

    public static void main(String[] args) {
        SpringApplication.run(SapBeApplication.class, args);
    }

}
